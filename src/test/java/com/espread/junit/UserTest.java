package com.espread.junit;

import com.espread.sys.model.SysUser;
import com.espread.sys.service.SysUserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-config.xml" })
public class UserTest {
	
	@Autowired
	private SysUserService sysUserService;
	
	@Test
	public void userTest() throws Exception{
		SysUser sysUser = sysUserService.findUserByLoginName("admin");
		System.out.println(sysUser.toString());
	}
}